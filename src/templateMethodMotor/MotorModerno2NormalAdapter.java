package templateMethodMotor;

public class MotorModerno2NormalAdapter extends MotorNormal{
	
	private MotorModerno motorModerno;
	
	MotorModerno2NormalAdapter(MotorModerno motorModerno){
		this.motorModerno = motorModerno;
	}

	@Override
	public void encender() {
		this.motorModerno.conectar();
		this.motorModerno.activar();
	}

	@Override
	public void acelerar() {
		this.motorModerno.moverMasRapido();
		
	}

	@Override
	public void apagar() {
		this.motorModerno.detener();
		this.motorModerno.desconectar();
	}

}
