package templateMethodMotor;

public abstract class MotorNormal {
	
	public void recorrido(){
		
		encender();
		acelerar();
		apagar();
		
	}
	
	public abstract void encender();
	
	public abstract void acelerar();
	
	public abstract void apagar();

}
