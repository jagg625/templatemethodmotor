package templateMethodMotor;

public abstract class MotorModerno{
	
public void recorrido(){
		
		conectar();
		activar();
		moverMasRapido();
		detener();
		detener();
		
	}
	
	public abstract void conectar();
	
	public abstract void activar();
	
	public abstract void moverMasRapido();
	
	public abstract void detener();
	
	public abstract void desconectar();

}
