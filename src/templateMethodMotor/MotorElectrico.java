package templateMethodMotor;

public class MotorElectrico extends MotorModerno{

	@Override
	public void conectar() {
		System.out.println("Conectar de un motor Electrico");
	}

	@Override
	public void activar() {
		System.out.println("Activar de un motor Electrico");
	}

	@Override
	public void moverMasRapido() {
		System.out.println("Moverse más rapdio de un motor Electrico");
		
	}

	@Override
	public void detener() {
		System.out.println("Deter de un motor Electrico");
		
	}

	@Override
	public void desconectar() {
		System.out.println("desconectar de un motor Electrico");
	}
}
