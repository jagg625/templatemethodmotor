package templateMethodMotor;

public class AutomovilNormal {
	
	private MotorNormal motorNormal;
	
	public AutomovilNormal(MotorNormal motorNormal){
		this.motorNormal = motorNormal;
	}
	
	public void encender(){
		this.motorNormal.encender();
	}
	
	public void acelerar(){
		this.motorNormal.acelerar();
	}
	
	public void apagar(){
		this.motorNormal.apagar();
	}
	
	public void cambiarMotor(MotorNormal motorNormal){
		this.motorNormal = motorNormal;
	}
	
	public void recorrido(){
		this.motorNormal.recorrido();
	}
}
