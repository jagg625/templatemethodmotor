package templateMethodMotor;

public class Main {
	
	private App app;

	public static void main(String[] args) {
		App app = new App();
		app.run();
	}
}
